import React from 'react';
import {renderToString} from 'react-dom/server';
import {StaticRouter as Router} from 'react-router-dom';

import FullPage from './../ClientApp/components/common/FullPage';


export default (req) => {
    let context = {};
    const content = renderToString(
        <Router location={req.path} context={context}>
            <FullPage />
        </Router>
    )

    return {
        htmlcode: `<html>
                        <head>
                            <title>One Click</title>
                            <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
                            <link rel="stylesheet" href="./assets/styles/bootstrap.min.css">
                            <link rel="stylesheet" href="./assets/styles/bootstrap.min.css.map">
                            <link rel="stylesheet" href="App.css">
                        </head>
                        <body>
                            <div id="root">${content}</div>
                            <script src="clientbundle.js"></script>
                        </body>
                  </html>`,
        routestatus: context.status
    }
}