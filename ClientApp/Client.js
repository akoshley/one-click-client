import React from "react";
import ReactDOM from "react-dom";
import FullPage from "./components/common/FullPage";
import {browserHistory} from 'react-router';
import {BrowserRouter as Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import configureStore from './../redux/configureStore';

const store = configureStore(window.__STATE__);

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <FullPage />
        </Router>
    </Provider>,
    document.getElementById("root")
);
