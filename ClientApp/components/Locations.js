import React, {Component} from 'react';
import { connect } from 'react-redux';
import {locationsFetchData} from './../../redux/actions/locationsActions';

class Locations extends Component {

    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
            appData: []
        }
    }

    componentDidMount(){

        this.props.locationsFetchData();

        // axios.get('data/projectLocations.json')
        //     .then((result) => {
        //         this.setState({
        //             appData: result.data,
        //             isLoading: false
        //         })
        //     })
        //     .catch( error => {
        //         if(error.response){
        //             console.log(error.responderEnd);
        //         }
        //     });
    }

    render(){
        if(this.props.isLoading){
            return <span><i>Loading...</i></span>
        }
        else if(this.props.hasErrored) {
            return <span><i>Failed to load data: {this.props.errorMessage}</i></span>
        }   
        else{

            let location = this.props.locations[0];
            return (
                <div>
                <h2>Locations</h2>
                <label> Name:</label> <span>{location.areaName}</span><br />
                <label> Area Code:</label> <span>{location.areaCode}</span> <br />
                <label> File name:</label> <span>{location.name}</span><br />
                <label> Version:</label> <span>{location.version}</span><br />
                <label> Last Modified:</label> <span>{location.lastModified}</span><br />
                <label> Last Modified By:</label> <span>{location.lastModifiedBy}</span><br />
                </div>
            );
         }
    }
}

Locations.propTypes = {};
Locations.defaultProps = {};

const mapStateToProps = (state) => {
    return {
            locations: state.locations.data,
            hasErrored: state.locations.hasErrored,
            isLoading: state.locations.isLoading,
            errorMessage: state.locations.errorMessage
        }
}

export default connect(mapStateToProps,
    {locationsFetchData}) (Locations)