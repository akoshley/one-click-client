import React from 'react'
import PropTypes from 'prop-types'

export default function PageTop(props) {

  return (
    <div>
      <header className="header">
        <div className="container-main align-items-center justify-content-between">
          {props.children}
        </div>
      </header>
    </div>
  );
}

PageTop.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
};