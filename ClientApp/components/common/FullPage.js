import React, {Component} from 'react';
import {Provider} from 'react-redux';

import Routes from '../../Routes';
import Menu from './Menu';
import PageTop from './PageTop';

class FullPage extends Component {

    constructor(props){
        super(props);
       this.handler = this.handler.bind(this);
    }

    handler(val) {
       this.props.action();
    }

    render() {       
            return (
                    <div>
                        <PageTop>
                            <Menu />
                        </PageTop>
                        <div className="container-fluid">
                        {/* <Routes /> */}
                        <Routes action={this.handler} />
                        </div>
                    </div>
            );
    }
}

FullPage.propTypes = {};
FullPage.defaultProps = {};

export default FullPage;

