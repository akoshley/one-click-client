import React from 'react'
import {Link} from 'react-router-dom';

export default function Menu() {
  return (
    <nav className="navbar navbar-expand navbar-dark bg-dark">
     <a className="navbar-brand" href="#">One Click</a>
        <ul className="navbar-nav justify-content-center">
            <li className="nav-item">
                <Link className="nav-link" to="/">Home</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/project">Project</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/locations">Locations</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/pid">2d</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/modelviewer">3d</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/snap">Snap</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/reports">Reports</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="/about">About</Link>
            </li>
        </ul>
  </nav>
  )
}
