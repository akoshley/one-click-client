import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div>
          <footer className="page-footer font-small dark">

            <div className="footer-copyright text-center py-3">© 2018 Copyright: Black and Veatch
            </div>
            </footer>
      </div>
    )
  }
}
