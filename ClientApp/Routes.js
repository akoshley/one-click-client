import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import Home from './components/Home';
import Project from './components/Project';
import Locations from './components/Locations';
import ModelViewer from './components/ModelViewer';
import Pid from './components/Pid';
import Reports from './components/Reports';
import About from './components/About';
import Snap from './components/Snap';
import RouteNotFound from './RouteNotFound';

class Routes extends Component {

    // constructor(props){
    //     super(props);
    //     this.handler = this.handler.bind(this);
    // }

    // handler() {
    //     this.props.action();
    // }        

    render () {
        return (
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/project" component={Project} />
                    <Route exact path="/locations" component={Locations} />
                    <Route exact path="/pid" component={Pid} />
                    <Route exact path="/modelviewer" component={ModelViewer} />
                    <Route exact path="/snap" component={Snap} />
                    <Route exact path="/reports" component={Reports} />
                    <Route exact path="/about" component={About} />
                    <Route render={() => <RouteNotFound /> }/>
                </Switch>
        );
    }
}


Routes.propTypes = {};
Routes.defaultProps = {};

export default Routes;