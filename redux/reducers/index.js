import {combineReducers} from 'redux';
import {locations} from './locationsReducers';

export default combineReducers({
    locations
});