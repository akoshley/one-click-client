import {
    LOCATION_LOAD,
    LOCATION_LOAD_SUCCESS,
    LOCATION_LOAD_FAIL
} from '../actions/locationsActions';

export function locations(state = {
    data: [],
    isLoading: true,
    hasErrored: false,
    errorMessage: ''
}, action) {
    switch (action.type) {
        case LOCATION_LOAD:
            return Object.assign({}, state, {
                isLoading: true,
                hasErrored: false
            });

        case LOCATION_LOAD_SUCCESS:
            return Object.assign({}, state, {
                data: action.payload.data,
                isLoading: false,
                hasErrored: false
            });

        case LOCATION_LOAD_FAIL:
            return Object.assign({}, state, {
                isLoading: false,
                hasErrored: true,
                errorMessage: action.error.message
            });

        default:
        return state;
    }

}