import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import reducers from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

const production = process.env.NODE_ENV &&
    process.env.NODE_ENV === "production";

if (!production) {
    require('dotenv').config();
}

const restUrl = production ?
    process.env.PROD_RESTURL :
    process.env.JSONSERVER_RESTURL;

let middleware = [
    thunk,
    axiosMiddleware(axios.create({baseURL:restUrl}))
];


if (!production) {
    middleware.push(require('redux-immutable-state-invariant').default());
    console.log('added redux-immutable-state-invariant');
}

export default function configureStore(initalState = {}) {

    const composeEnhancers = composeWithDevTools({
    });

      const client = axios.create({ 
        baseURL: restUrl,
        responseType: 'json'
    });

  return createStore(
      reducers,
      initalState,
      composeEnhancers(
        applyMiddleware(...middleware))
    );
}
