export const LOCATION_LOAD = 'LOCATION_LOAD';
export const LOCATION_LOAD_SUCCESS = 'LOCATION_LOAD_SUCCESS';
export const LOCATION_LOAD_FAIL = 'LOCATION_LOAD_FAIL';

export function locationsFetchData () {
    return {
        type: LOCATION_LOAD,
        payload: {
            request: {
                //url: '/data/projectLocations.json'
                url: '/locations'
            }
        }
    }
}
