# Introduction 
This project is a React/Redux client for One-Click REST APIs.

# Getting Started
1.	Installation process
    -- From project root directory: run command ---> npm install

2.	Software dependencies
    -- This project requires you to have node installed on your machine.
    
3.	Latest releases
    -- Release 1.0

4.	API references
    -- TODO

# Build and Test
    -- To transpile and build: ---> webpack
    -- To start client: ---> npm run start:dev

